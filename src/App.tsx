import React from 'react';
import './App.css';
import { SignIn } from './components/SignIn';
import { Navbar } from './components/Navbar/Navbar';
import { Home } from './components/Home';
import { Reseaux } from './components/Reseaux';
import { Shop } from './components/Shop';
import { Profile } from './components/Profile';
import { useState } from 'react';
import { AuthContext } from "./context/context";
import { PrivateRoute } from './components/utils/PrivateRoute';
import { User } from './components/User';
import axios from 'axios';

import {
  Route,
  Routes,
  useNavigate
} from "react-router-dom";

const App: React.FC = () => {
  const [token, setToken] = React.useState<string | null>(null)
  const [basketnB, setBasketnB] = React.useState<number>(0)
  const [basket, setBasket] = React.useState<any>([])
  const navigate = useNavigate()

  const handleLogin = (token: string):void => {
    setToken(token)
    navigate('/')
  }

  const resetShopping = ():void => {
    setBasketnB(0)
    setBasket([])
  }

  const handleShopping = (id: string, name: string, price: number, type='add'):void => {
    if (type === "add") {
      setBasketnB(basketnB + 1)
      const search = (element: any) => element.id === id;
      const productIndex = basket.findIndex(search)
      if (productIndex !== -1) {
        let copy = [...basket]
        const unitPrice = copy[productIndex].price / copy[productIndex].quantity
        const newQuantity = copy[productIndex].quantity + 1
        const newPrice = newQuantity * unitPrice
        copy[productIndex].quantity = newQuantity
        copy[productIndex].price = newPrice
        setBasket(copy)
      } else {
        const product: any = {
          id: id,
          name: name,
          price: price,
          quantity: 1
        }
        let copy = [...basket]
        copy.push(product)
        setBasket(copy)
      }
    } else {
      setBasketnB(basketnB - 1)
      const search = (element: any) => element.id === id;
      const productIndex = basket.findIndex(search)
      if (productIndex !== -1) {
        let copy = [...basket]
        const unitPrice = copy[productIndex].price / copy[productIndex].quantity
        const newQuantity = copy[productIndex].quantity - 1
        const newPrice = newQuantity * unitPrice
        copy[productIndex].quantity = newQuantity
        copy[productIndex].price = newPrice
        if (copy[productIndex].quantity === 0) {
          copy.splice(productIndex, 1)
        }
        setBasket(copy)
      }
    }
  }

  const handleLogout = () => {
    setToken(null)
  }

  return (
    <>
      {/* <div>
        <h1>Brouillon test technique</h1>
      </div>
      <button onClick={() => {console.log("papa")}}>ui</button> */}
      <AuthContext.Provider value={{token}}>
          <Routes>
            <Route path="/signin" element={<SignIn handleLogin={handleLogin}/>}/>
            <Route element={<Navbar basketNb={basketnB} basket={basket} handleShopping={handleShopping} resetShopping={resetShopping}/>}>
              <Route path="/" element={<Home/>}/>
              <Route path="/reseaux" element={<Reseaux/>}/>
              <Route path="/profile" element={<Profile/>}/>
              <Route path="/boutique" element={<Shop basketNb={basketnB} handleShopping={handleShopping}/>}/>
              <Route path="/user" element={<PrivateRoute><User/></PrivateRoute>}/>
            </Route>
          </Routes>
      </AuthContext.Provider>
    </>
  );
}


export default App;
