import { createContext, useContext } from 'react';
import React from 'react';

export const AuthContext = React.createContext<any>(null);
export function useAuth() {
  return useContext(AuthContext);
}
