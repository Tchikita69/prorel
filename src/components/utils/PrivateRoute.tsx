import React from 'react'
import { Route, useNavigate, Navigate } from 'react-router-dom';
import { useAuth } from "../../context/context";

export const PrivateRoute = ({children}: any) => {
    const { token } = useAuth();
    
    if (!token) {
        return <Navigate to="/signin" replace/>;
    }
    return children
        
}

