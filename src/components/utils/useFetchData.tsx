import { useEffect, useState} from 'react';
import axios, { AxiosRequestHeaders } from 'axios';

const useFetchData = (url: string, params: object, headers: AxiosRequestHeaders) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const { data: response} = await axios.get(url, {params: params, headers: headers});
        console.log(response)
        setData(response);
      } catch (error) {
        console.error(error)
      }
      setLoading(false);
    };

    fetchData();
  }, []);

  return {
    data,
    loading,
  };
};

export default useFetchData;