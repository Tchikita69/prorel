import React from 'react'
import './Home.css'
import logo from '../ProrelLogoColor.svg'
import img from '../prorel-whitetext.png'
import {
    Link
} from "react-router-dom";

export const Home = () => {
    return (
        <div className="home">
            <img src={logo} className="home-logo" alt="logo" />
            <img src={img} className="home-logotext"/>
            <Link to="/reseaux">
                <button className="submit">
                    Découvrir les réseaux
                </button>
            </Link>
        </div>
    )
}
