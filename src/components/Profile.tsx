import React from 'react'
import { useState} from 'react';
import {
    useLocation,
    useNavigate
} from "react-router-dom"
import './Profile.css'
import useFetchData from './utils/useFetchData'
import parse from "html-react-parser";

const actualités = [
    {
        id: 0,
        title: "Un plugin incroyable",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quia consequatur quam aliquid quas, quos sed inventore atque tempore deserunt animi saepe sit quisquam nostrum quae labore? Aliquam nisi eos ad. Tempore architecto deleniti iusto placeat quasi fugiat. Cupiditate consequuntur, rerum nulla illum ut debitis nesciunt magnam nemo doloribus quibusdam unde repellat necessitatibus vel quasi inventore enim corporis alias deserunt quae dolorum quos cumque maiores delectus? Recusandae nesciunt molestias doloribus qui dolores ratione sequi enim suscipit incidunt laborum exercitationem consequuntur assumenda error adipisci reprehenderit sapiente ipsam, omnis voluptate, deserunt consequatur dicta consectetur iusto? Voluptatum repudiandae quisquam animi quia in adipisci consequatur assumenda ducimus ut ratione mollitia molestias, aperiam doloremque iure aliquid fugiat. Saepe, cum, vel reiciendis soluta iste ducimus sunt unde sint laboriosam rerum earum quod nihil accusamus molestiae, hic ab! Dolore ab possimus veniam, sed similique perferendis incidunt animi impedit est enim beatae tempore illo hic soluta tenetur quo inventore. Recusandae, unde id fuga blanditiis beatae sapiente ad eos autem eum rerum officia dolores! Nesciunt, nostrum dolorum labore beatae eos vel cupiditate unde odio delectus quos id doloremque, officiis perferendis. Eaque ut quidem optio dolor nobis expedita recusandae eveniet maxime. Necessitatibus tenetur repellendus modi sint, molestiae vel labore consequatur, praesentium dolorem porro fuga. Tempora architecto inventore atque soluta quis fuga accusamus ullam, natus praesentium maiores qui cupiditate vitae necessitatibus. Ipsam fuga nisi officiis! Alias nesciunt reiciendis fugit suscipit et? Sunt facere facilis doloremque quas odit quisquam molestias culpa neque placeat aut, hic modi possimus tempore nisi laboriosam praesentium omnis provident repellat magnam tempora rem et recusandae aliquid? Provident quasi consectetur odio. Facere totam commodi voluptas temporibus molestiae natus quam beatae. Mollitia repellat harum, dolorem perferendis doloribus magni voluptas. Illo quas, ipsam ex doloribus laudantium laborum totam id dicta eligendi labore libero officiis distinctio molestias dolore fugiat! Alias sint quo repellendus voluptatum explicabo molestias corrupti magni! At consequuntur totam, sed reprehenderit, debitis in explicabo voluptatem ipsa deleniti aut, perspiciatis maiores. Odio sunt saepe magni harum numquam doloremque similique fugiat reprehenderit animi quam consectetur enim, ipsam voluptatem distinctio, quas repellendus eveniet nulla ad facere earum commodi autem esse labore recusandae. Doloribus inventore ab temporibus officiis porro enim corporis ad, non sit. Ipsum magnam voluptate beatae! Facilis quis consequatur, laudantium itaque, a suscipit iure ullam natus reiciendis accusantium sapiente neque unde recusandae voluptas aperiam, quidem quod ut obcaecati hic saepe adipisci pariatur autem? Nam fugit cupiditate magnam, nisi quasi voluptatum. Nemo, repudiandae id autem est sapiente voluptates qui ab incidunt laborum cumque ipsam ratione nobis placeat dolorem vel aut harum dolor dignissimos rerum soluta a aliquid dolorum natus! Ipsam ipsa repudiandae nobis eius est libero fugit dolorum, illum assumenda eum exercitationem repellat. Quia inventore rem blanditiis voluptates in? Quia repellat velit, excepturi aut harum reprehenderit fugit eaque obcaecati numquam ut quisquam quibusdam tenetur tempore, illum, impedit accusantium odit dolorem similique assumenda aspernatur illo eius. A, provident repellat fugiat reiciendis quos suscipit dolorem soluta vitae alias, dicta temporibus pariatur explicabo deserunt totam illo aliquam, sint excepturi voluptatibus porro! In veniam vitae omnis error maxime."
    },
    {
        id: 1,
        title: "Quelques news les loulous !",
        content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam in asperiores consectetur exercitationem mollitia cupiditate, nesciunt praesentium ex dignissimos ipsum! Nobis consequuntur dolore totam quod quis rem, cupiditate molestiae vel impedit error ratione, quaerat possimus velit harum porro voluptatem cumque, iure numquam. Deserunt harum nemo odit accusamus repudiandae nulla assumenda magni eius ipsum, mollitia sint dignissimos excepturi tempore deleniti optio eos qui id voluptatibus corporis quo tempora maxime quasi eum! Dolor excepturi ullam quos, nulla repudiandae necessitatibus ex odio, vitae recusandae earum veritatis distinctio corporis iste temporibus delectus consequuntur laboriosam tenetur illum! Possimus unde provident, facilis repudiandae corrupti architecto iure at cum molestias quisquam ipsa adipisci. Accusamus ex modi omnis quod atque similique dicta, provident tempore eius doloremque cumque quibusdam suscipit iure! Dolorum officia deserunt iure libero rem, tempore deleniti, nisi, magnam maxime hic neque! Minus maxime recusandae iste cupiditate! Quibusdam harum aliquam maiores mollitia soluta et magnam, tenetur atque? Molestias tempore pariatur cumque atque neque illo inventore obcaecati minima ipsum et accusamus ratione ab, quasi ex ea minus. Quo laudantium esse labore ut obcaecati temporibus odio at facilis dolorum nisi. Sint architecto velit, ratione odio a voluptate! At, labore illum quas error dolore nisi reiciendis hic expedita, optio maiores possimus molestiae sed provident itaque temporibus harum quisquam omnis! Quaerat delectus omnis doloremque alias hic eum temporibus nemo! Esse perspiciatis architecto tempora reiciendis in. Explicabo recusandae sapiente magnam mollitia quisquam velit dolorum neque alias nisi tempore atque ex similique doloribus, aspernatur labore pariatur? Cum expedita est eius, iure totam consectetur ad et vitae odit asperiores autem eum ut tenetur, inventore porro corporis recusandae voluptatum temporibus iste fugit amet ab vero nihil accusamus. Asperiores iste beatae libero, repudiandae distinctio dicta enim fugiat rem pariatur consequuntur, minima modi atque molestias quo quos error illo iure id dolorem omnis neque sit? Qui praesentium iusto nihil beatae quidem aperiam officiis nemo est iure natus assumenda, laboriosam amet, odio perferendis ipsam quod totam blanditiis delectus autem maxime odit, vero suscipit. Ipsa cumque cum obcaecati, facilis blanditiis incidunt laudantium neque, corporis quod sed numquam placeat, aspernatur similique hic earum provident quasi! Ad cumque modi, saepe dolores repellat a voluptatibus assumenda ipsa explicabo, eos, distinctio similique vitae accusantium quis illo mollitia iste corporis. Perferendis perspiciatis vero aspernatur earum molestias harum. Ipsa atque commodi, officiis aspernatur voluptas, accusantium debitis porro nam, ullam voluptates accusamus fugit mollitia ab reiciendis molestias cupiditate sequi incidunt magni velit esse quasi perferendis vitae. Et exercitationem expedita sunt voluptate facilis deleniti molestiae impedit pariatur, laborum magni blanditiis. Accusantium unde dolore eius distinctio eum ut illum deserunt, repellendus id enim fugiat veniam ad repellat quis non labore asperiores sapiente alias nemo qui, expedita illo odit? Ipsam animi illo distinctio odit. Atque ipsa suscipit esse reiciendis repudiandae numquam tenetur, placeat amet aliquam consequuntur dolore eum delectus animi ad veniam quos? Dolore aut, aliquam in laudantium accusamus minima provident alias! Praesentium vel animi maiores excepturi ipsum in fuga reprehenderit placeat commodi, soluta ipsa sint, optio, quae veritatis modi minus? Delectus reprehenderit, iste doloribus magnam labore corporis tenetur esse velit molestias accusantium consequatur earum pariatur quae repellendus! Consequatur exercitationem, eveniet voluptatibus possimus animi praesentium nihil fugit! Corrupti commodi voluptatum culpa nesciunt suscipit ducimus voluptatem, eos eveniet quasi placeat accusamus inventore totam eligendi. Quaerat quas, in officia laboriosam ipsa fugiat, unde vitae corporis rerum cum commodi alias incidunt ratione consectetur id quidem cumque tempora. Nemo modi nulla iusto cupiditate hic? Cupiditate aut quod eos id atque tempora odio animi, architecto ducimus quas, saepe et expedita consequatur ex nihil, dicta mollitia. Cumque natus debitis ut laudantium cupiditate illo! Quis reprehenderit repellat debitis, provident molestiae numquam. Quae consectetur sit animi facere, necessitatibus molestiae quis ea reprehenderit quaerat perferendis sapiente explicabo debitis labore itaque, odit nemo id omnis? Molestias maiores ipsam, rem eos explicabo quasi saepe dicta nesciunt repudiandae amet animi, libero veritatis delectus? Sed enim vero animi, facere at itaque qui atque delectus, blanditiis deleniti odit, veritatis beatae numquam neque nulla a sunt fuga molestiae id quidem eveniet esse. Eius sunt, suscipit porro praesentium illo magnam dicta totam voluptatibus accusamus autem officia nobis hic sapiente asperiores deleniti rerum delectus neque ducimus enim nesciunt harum mollitia dignissimos? Delectus quo odio animi ducimus corrupti excepturi magni porro assumenda perferendis doloribus itaque eveniet voluptas, eaque hic quas! Sed fugit velit distinctio esse tenetur quibusdam explicabo deleniti dicta quam doloremque odit nemo, quasi debitis dignissimos expedita temporibus vitae fuga et deserunt. Omnis, voluptas? Aperiam quasi odit placeat, officiis alias libero. Quas, provident sit eaque, voluptatem aspernatur facilis odio sequi sunt in, ipsam ipsa eligendi dolores debitis libero accusamus totam alias rerum! Est necessitatibus exercitationem esse expedita in tempora rerum iusto obcaecati officia ipsa dignissimos, blanditiis, ut deserunt cum. Praesentium commodi fuga corporis ipsum quod et voluptatum rerum placeat nam. Asperiores nulla voluptatibus deserunt adipisci. Animi libero rem vitae vel atque numquam. Consequatur repellat distinctio laudantium quae, vel ducimus! Quidem quo eligendi id beatae architecto autem harum vero eaque consectetur quam alias, eveniet, exercitationem nobis atque eos sed laborum in, maiores pariatur animi magni illo. Soluta fugit totam blanditiis aut. Tempora, repellendus nemo. Deserunt eius corporis ipsum earum veritatis laborum incidunt impedit repellendus dolorum odio cumque delectus perspiciatis assumenda, possimus dolores ex a temporibus labore illum eum! Illo iusto ipsum neque facere? Nostrum consequuntur sapiente quod. Doloribus quod iure tenetur totam impedit esse quos repellendus sapiente tempore mollitia fuga, doloremque unde ut corporis quae earum illum. Numquam, dignissimos sit tempora, possimus delectus minima nihil, ipsam fugiat mollitia quam aspernatur quos nulla quo quia repellat eius recusandae eveniet. Maiores sapiente totam maxime ipsum laboriosam debitis cum officiis! Nesciunt voluptate sapiente ullam animi, perspiciatis omnis recusandae porro earum maiores molestias adipisci libero eius numquam voluptates quaerat. Quod quibusdam itaque exercitationem sit nisi nesciunt quo fugit incidunt asperiores repellendus quae maiores, mollitia in laboriosam porro esse eaque facere numquam nobis consequatur nostrum dicta. Voluptates doloremque eum nam modi voluptatibus, voluptas facilis, optio rem cupiditate mollitia, id error dolor? Suscipit nostrum veniam, ullam unde laudantium, possimus eum ipsum dolorum officia et culpa minima, iusto tenetur impedit ducimus quisquam itaque quae."
    }
]

export const Profile = () => {
    const location:any = useLocation()
    const fetchActivityPages:any = useFetchData('http://localhost:3030/visitor/activityPage', {id: location.state.id}, {});
    const [activeLink, setActiveLink] = useState('Activités')
    const navigate = useNavigate()

    const handleShopNav = (userId: number, username: string) => {
        // console.log(userId)
        // useNavigate()
        navigate('/boutique', {state: {userId, username}})
    }

    return (
        <div className="profile-page">
            <div className="card">
                <div className="card-image">
                    <img src="./images/cover.jpg" alt=""/>
                </div>
                <div className="profile-image">
                    <img src="./images/profile.jpg" alt=""/>
                </div>
                {(!fetchActivityPages.loading && fetchActivityPages.data.page !== undefined) &&
                <div className="card-content">
                    <h3>{fetchActivityPages.data.page.name} - {fetchActivityPages.data.page.title}</h3>
                    <p>Les as de la vidéo</p>
                </div>
                }
            </div>
            <div className="profile-content">
                <div className="profile-navigation">
                    <p onClick={() => setActiveLink('Activités')}>Activités</p>
                    <p onClick={() => setActiveLink('Actualités')}>Actualités</p>
                    <p onClick={() => handleShopNav(location.state.id, location.state.name)}>Mes produits / Mes services</p>
                </div>
                <div className="profile-sections">
                    {(!fetchActivityPages.loading && fetchActivityPages.data.page !== undefined && activeLink === 'Activités') && 
                    <div className="sections">
                        {
                            fetchActivityPages.data.page.sections.map((item: any, index: React.Key | null | undefined) =>{
                                return (
                                <div className="section-preview" key={index}>
                                    <h2>{item.title}</h2>
                                    <div>
                                        {parse(item.body)}
                                    </div>
                                </div>
                                )
                            })
                        }
                    </div>}
                    {activeLink === 'Actualités' &&
                    <div className="sections">
                        {
                            actualités.map((item: any, index: React.Key | null | undefined) =>{
                                return (
                                    <div className="section-preview" key={index}>
                                    <h2>{item.title}</h2>
                                    <div>
                                        {parse(item.content)}
                                    </div>
                                </div>
                                )
                            })
                        }
                    </div>
                    }
                </div>
            </div>
        </div>
    )
}
