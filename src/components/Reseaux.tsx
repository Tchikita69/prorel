import React from 'react'
import './Reseaux.css'
import useFetchData from './utils/useFetchData'
import {
    useNavigate
} from "react-router-dom"

const categories = [
    {
        title: 'Informatique',
        number: 10,
        id: 0
    },
    {
        title: 'Bien être',
        number: 2,
        id: 1
    },
    {
        title: 'Cosmétique',
        number: 3,
        id: 2
    },
    {
        title: 'Vidéo',
        number: 12,
        id: 3
    },
    {
        title: 'Musique',
        number: 23,
        id: 4
    },
    {
        title: 'Médecine',
        number: 8,
        id: 5
    },
    {
        title: 'Bricolage',
        number: 2,
        id: 6
    },
    {
        title: 'Design',
        number: 3,
        id: 7
    },
]

const networks = [
    {
        title: "Les as de la vidéo",
        type: "Services audiovisuels",
        presentation: "Les as de la vidéo c’est avant tout une équipe de passionnés et d’amis originaires de Lyon. Notre équipe est complète et qualifiée pour tout type de réalisation audiovisuelles. Nous avons déjà travaillé pour des émissions sur TF1 ou M6, mais aussi sur des bandes annonces de films connus comme “Qu’est ce qu’on a fait au bon dieu 8”.",
    }
]

// interface getRequest {
//     data: any,
//     loading: boolean
// }

export const Reseaux = (setProfileId: any) => {
    const fetchActivityPages:any = useFetchData('http://localhost:3030/visitor/activityPage/previews', {id: 1}, {});
    
    const navigate = useNavigate()
    const handleProfileNav = (id: number, name: string) => {
        navigate('/profile', {state: {id, name}})
    }

    return (
        <div className="reseaux">
            <div className="categories-bar">
                <p className="categories-title">Categories</p>
                <ul className='categories'>
                    {categories.map((item, index) => {
                        return (
                            <li key={index} className="category">{item.title} ({item.number})</li>
                        )
                    })}
                </ul>
            </div>
            <div className="networks">
                <p className="networks-title">Réseaux</p>
                <p className="networks-description">Découvrez tous les réseaux de professionnels utilisant actuellement Prorel. Suivez leurs actualités et leur contenu et accédez à leurs produits ou services en toute simplicité.</p>
                <select className="filters">
                    <option value="popularity">Plus populaires</option>
                    <option value="date1">Du plus récent au moins récent</option>
                    <option value="date2">Du moins récent au plus récent</option>
                    <option value="note">Les mieux notés</option>
                </select>
                <div className="networks-cards">
                    {networks.map((item, index) => {
                        return (
                            <div className="network-card" key={index}>
                                <h3 className="card-title">{item.title}</h3>
                                <h4 className="card-type">{item.type}</h4>
                                <div className="card-container">
                                    <div className="card-top">
                                        <div className="card-photo">
                                            No photo
                                        </div>
                                        <div className="card-presentation">
                                            <h4 className="presentation-title">Presentation</h4>
                                            {item.presentation}
                                        </div>
                                    </div>
                                    <div className="card-team">
                                        <h4 className="presentation-title">L'équipe</h4>
                                        <div className="team-members">
                                            {fetchActivityPages.loading && <p>Loading...</p>}
                                            {(!fetchActivityPages.loading && fetchActivityPages.data.previews !== undefined) && 
                                                <ul className='members'>
                                                    {fetchActivityPages.data.previews.map((item: any, index: React.Key | null | undefined) => {
                                                        return (
                                                            <div onClick={() => handleProfileNav(item.id, item.name)} key={index} className="member-container">
                                                                <li className="member-text">{item.name}</li>
                                                                <p>{item.preview}</p>
                                                            </div>
                                                        )
                                                    })}
                                                </ul>}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        </div>
    )
}
