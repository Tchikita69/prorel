import React from 'react'
import { useState, useEffect } from 'react'
import './Shop.css'
import {
    useLocation
} from "react-router-dom"
import axios, { AxiosRequestHeaders } from 'axios';
import { useAuth } from "../context/context"

const categories = [
    {
        title: 'Informatique',
        number: 10,
        id: 0
    },
    {
        title: 'Bien être',
        number: 2,
        id: 1
    },
    {
        title: 'Cosmétique',
        number: 3,
        id: 2
    },
    {
        title: 'Vidéo',
        number: 12,
        id: 3
    },
    {
        title: 'Musique',
        number: 23,
        id: 4
    },
    {
        title: 'Médecine',
        number: 8,
        id: 5
    },
    {
        title: 'Bricolage',
        number: 2,
        id: 6
    },
    {
        title: 'Design',
        number: 3,
        id: 7
    },
]

interface ShoppingProps {
    basketNb: number,
    handleShopping: (id: string, name: string, price: number) => void
}

export const Shop : React.FC<ShoppingProps>= ({basketNb, handleShopping}) => {
    const location: any = useLocation()
    const [result, setResult] = useState([])
    const [loading, setLoading] = useState(true)
    const [forbidden, setForbidden] = useState(true)
    const { token } = useAuth();
    
    const fetchData = async (url: string, params: any, headers: any) => {
        try {
          const { data: response} = await axios.get(url, {params: params, headers: headers});
          if (location.state !== null) {
              filterShop(response.products)
          } else {
              setResult(response.products)
              console.log(result)
              setLoading(false)
          }
        } catch (error) {
          console.error(error)
        }
      };

    const filterShop: any = (data: any) => {
        const idFilter = location.state.userId
        let filtered: any = []

        data.forEach((element: { sellerId: unknown; }) => {
            if (element.sellerId === idFilter) {
                filtered.push(element)
            }
        })
        setResult(filtered)
        setLoading(false)
    }

    useEffect(() => {
        if (token) {
            setForbidden(false)
        }
        fetchData('http://localhost:3030/visitor/products', {}, {});
      }, []);

    
    return (
        <div className="shop">
            <div className="categories-bar">
                <p className="categories-title">Categories</p>
                <ul className='categories'>
                    {categories.map((item, index) => {
                        return (
                            <li key={index} className="category">{item.title} ({item.number})</li>
                        )
                    })}
                </ul>
            </div>
            <div className="shop-content">
                <p className="networks-title">La boutique</p>
                <p className="networks-description">Découvrez les produits et les prestations des utilisateurs de Prorel.</p>
                <div className="active-filters">
                    <h2>Filters:</h2>
                    <p>{location.state ? location.state.username : 'None'}</p>
                </div>
                <div className="shop-filters">
                    <select className="filters">
                        <option value="popularity">Plus populaires</option>
                        <option value="date1">Du plus récent au moins récent</option>
                        <option value="date2">Du moins récent au plus récent</option>
                        <option value="note">Les mieux notés</option>
                    </select>
                    <select className="filters">
                        <option value="popularity">-- Type --</option>
                        <option value="date1">Produits</option>
                        <option value="date2">Prestations</option>
                        <option value="note">Autres</option>
                    </select>
                </div>
                <div className="shop-cards-container">
                    {loading && <p>Loading...</p>}
                    {(!loading && result !== undefined && result !== null)&&
                        <ul className="shop-cards">
                        {result.map((item: any, index: React.Key | null | undefined) => {
                            return (
                                <div className="product-card" key={index}>
                                    <div className="product-card-image">
                                        <img src="./images/cover.jpg" alt=""/>
                                    </div>
                                    <div className="product-bottom">
                                        <div className="product-firstline">
                                            <h2>{item.name}</h2>
                                            <h2>{item.price} $</h2>
                                        </div>
                                        <div className="product-secondline">
                                            <div className="product-author">
                                                <p>Service proposé par Yves WARRIEN</p>
                                                <p className="product-network">Les as de la vidéo</p>
                                            </div>
                                            <button className="add" onClick={() => forbidden ? alert("Vous devez vous connecter pour effectuez des achats") : handleShopping(item._id, item.name, item.price)}>Ajouter au panier</button>
                                        </div>
                                        <div className="product-lastline">
                                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, eos sunt incidunt deserunt accusamus necessitatibus est, assumenda molestiae sequi rerum perferendis itaque quis maxime atque ea aliquam. Numquam totam facere veritatis! Nisi tenetur eius vero fugit expedita, facere officia delectus fugiat modi nesciunt tempora animi quisquam cupiditate doloribus incidunt sequi dolorem debitis aperiam rem! Nobis eius tenetur sint odio amet tempora deserunt explicabo quaerat distinctio necessitatibus voluptate iusto non, at ducimus ipsum dolore magni odit? Ullam iure magni labore tempora quibusdam fugit, ratione quidem illo, similique distinctio nulla, laboriosam reprehenderit perspiciatis error dicta. Voluptas maxime minus tempore vel quos laborum est perspiciatis dignissimos delectus error, amet et debitis labore fugit repudiandae odio consectetur voluptatem. Iste ipsam doloremque magnam quas ab!</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                        </ul>
                    }
                </div>
            </div>
        </div>
    )
}
