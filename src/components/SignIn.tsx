import React from 'react'
import './SignIn.css';
import { useState} from 'react';
import axios from 'axios';

interface Props {
    handleLogin: (token: string) => void
}

export const SignIn: React.FC<Props> = ({handleLogin}) => {
    const [signIn, setSignIn] = useState<boolean>(true)
    const [username, setUsername] = useState<string>('')
    const [password, setPassword] = useState<string>('')
    const [password2, setPassword2] = useState<string>('')

    const signUpRequest = async (username: string, password: string) => {
        const url = 'http://localhost:3030/visitor/register'
        const params = {
            username: username,
            password: password
        }

        try {
          const { data: response} = await axios.post(url, params);
          console.log('Account created successfully, will create a toast soon.')
          resetFields()
          setSignIn(true)
        } catch (error) {
          console.error(error)
        }
    }
    
    const signInRequest = async (username: string, password: string) => {
        const url = 'http://localhost:3030/visitor/login'
        const params = {
            username: username,
            password: password
        }

        try {
          const { data: response} = await axios.get(url, {params: params});
          resetFields()
          console.log(response)
          handleLogin(response.token)
        } catch (error) {
          console.error(error)
        }
    }

    const resetFields = () => {
        setUsername('')
        setPassword('')
        setPassword2('')
    }

    return (
        <div className="signin">
            <div className="signin-side">
                <h1>Plus d'une centaine de réseaux</h1>
                <video className="background-video" autoPlay loop muted>
                    <source src="./videos/People2.mp4" type="video/mp4"/>
                </video>
            </div>
            <div className="main">
                    {
                        signIn && 
                        <div className="login-container">
                            <p className="title">Se connecter</p>
                            <div className="separator"></div>
                            <p className="welcome-message">Entrez votre identifiant et mot de passe afin d'accéder à nos services</p>
                            <form className="login-form">
                                <div className="form-control">
                                    <input value={username} onChange={(e) => {setUsername(e.target.value)}} type="text" placeholder="Adresse mail"/>
                                    <i className="fas fa-user"></i>
                                </div>
                                <div className="form-control">
                                    <input value={password} onChange={(e) => {setPassword(e.target.value)}} type="password" placeholder="Mot de passe"/>
                                    <i className="fas fa-lock"></i>
                                </div>
                                <button className="submit" onClick={() => {signInRequest(username, password)}}>Se connecter</button>
                            </form>
                                <a href="#">Mot de passe oublié.</a>
                            <div className="login-signup">
                                <a href="#" onClick={() => {setSignIn(false);resetFields()}}>Pas encore de compte ? Rejoignez-nous</a>
                            </div>
                        </div>
                    }
                    {
                        !signIn && 
                        <div className="login-container">
                            <p className="title">S'inscrire</p>
                            <div className="separator"></div>
                            <p className="welcome-message">Inscrivez-vous et profitez pleinement des avantages de Prorel.</p>
                            <form className="login-form">
                                <div className="form-control">
                                    <input value={username} onChange={(e) => {setUsername(e.target.value)}} type="text" placeholder="Adresse mail"/>
                                </div>
                                <div className="form-control">
                                    <input value={password} onChange={(e) => {setPassword(e.target.value)}} type="password" placeholder="Mot de passe"/>
                                </div>
                                <div className="form-control">
                                    <input value={password2} onChange={(e) => {setPassword2(e.target.value)}} type="password" placeholder="Confirmer votre mot de passe."/>
                                </div>
                                <button className="submit" onClick={() => {signUpRequest(username, password)}}>Créer mon compte</button>
                            </form>
                            <div className="login-signup">
                                <a onClick={() => {setSignIn(true);resetFields()}} href="#">Déjà inscrit ? Connectez-vous à votre compte</a>
                            </div>
                        </div>
                    }
            </div>
        </div>
    )
}
