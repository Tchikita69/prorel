import React from 'react'
import './User.css'

export const User = () => {
    return (
        <div className="user-page">
            <div className="user-container">
                <p className="networks-title">Paramètres de mon compte</p>
                <form className="userparams-form">
                        <div className="form-control">
                            <input type="text" placeholder="Prénom"/>
                        </div>
                        <div className="form-control">
                            <input type="text" placeholder="Nom"/>
                        </div>
                        <div className="form-control">
                            <input type="text" placeholder="Confirmer votre mot de passe."/>
                        </div>
                        <button>Sauvegarder mes informations</button>
                </form>
            </div>
        </div>
    )
}

export default User
