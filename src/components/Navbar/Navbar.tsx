import React from 'react'
import { MenuItemsUser, MenuItemsVisitor } from './MenuItem'
import './Navbar.css';
import { Outlet } from 'react-router-dom';
import {
    Link
} from "react-router-dom";
import logo from '../../ProrelLogoColor.svg'
import { faXmark, faBars, faBasketShopping } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useAuth } from "../../context/context";

interface NavbarProps {
    basketNb: number,
    basket: any,
    handleShopping: (id: string, name: string, price: number, type: string) => void
    resetShopping: () => void
}
export const Navbar: React.FC<NavbarProps> = ({basketNb, basket, handleShopping, resetShopping}) => {
    const [clicked, setClicked] = React.useState(true)
    const [displayBasket, setDisplayBasket] = React.useState(false)

    const { token } = useAuth();

    const getTotal = ()  => {
        let total = 0
        basket.forEach((element: { price: number; }) => {
            total += element.price
        })
        return total
    }

    const finalizeShopping = () => {
        alert("Achat effectué avec succès !")
        resetShopping()
    }
    // console.log(token)
    return (
        <div className="layout">
            <nav className="NavbarItems">
                <img src={logo} className="navbar-logo" alt="logo"/>
                <div className="nav-menu">
                    <ul className={clicked ? 'nav-menutitles active' : 'nav-menutitles'}>
                        {
                            (token ? MenuItemsUser : MenuItemsVisitor).map((item, index) => {
                                return (
                                    <li className="navbar-text" key={index}>
                                        <Link className={item.cName} to={item.url}>
                                            {item.title}
                                        </Link>
                                    </li>
                                )
                            })
                        }
                    </ul>
                    <div className="menu-icons">
                        <div className="basket" onClick={() => setDisplayBasket(!displayBasket)}>
                            <FontAwesomeIcon icon={faBasketShopping} />
                            {basketNb > 0 && <span className="w3-badge w3-red">{basketNb}</span>}
                        </div>
                        {displayBasket && <div className='basket-container'>
                                            <h3>Panier</h3>
                                            {basket.forEach((item: any, index: number) => {
                                                return (
                                                    <div className="basket-product" key={index}>
                                                        <div className="basket-product-left">
                                                            <h5>
                                                                {item.name} 
                                                            </h5>
                                                            <div className="basket-quantity-container">
                                                                <div>
                                                                    <p>Quantité:</p>
                                                                </div>
                                                                <div className="basket-quantity-buttons">
                                                                    <button onClick={() => {handleShopping(item.id, item.name, item.price, 'remove')}}>-</button>
                                                                    {item.quantity}
                                                                    <button onClick={() => {handleShopping(item.id, item.name, item.price, 'add')}}>+</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="basket-product-right">
                                                            {item.price} $
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                            {basket.length > 0 &&  <button onClick={() => finalizeShopping()}style={{fontSize: "medium", padding: 10}}>Confirmer achat: {getTotal()} $</button>}
                                            {basket.length === 0 && <p style={{fontStyle: 'italic', fontSize: 'smaller'}}>Aucun article dans le panier</p>}
                                        </div>}
                        <div className="menu-icon" onClick={() => setClicked(!clicked)}>
                            <FontAwesomeIcon icon={clicked ? faXmark : faBars} />
                        </div>
                    </div>
                </div>
            </nav>
            <main>
                <Outlet/>
            </main>
            <footer>© COPYRIGHT 2022 - Tous Droits Réservés</footer>
        </div>
    )
}
