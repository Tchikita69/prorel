import React from 'react'

export const MenuItemsUser = [
    {
        title: 'Reseaux',
        url: '/reseaux',
        cName: 'nav-links'
    },
    {
        title: 'Boutique',
        url: '/boutique',
        cName: 'nav-links'
    },
    {
        title: 'Mon compte',
        url: '/user',
        cName: 'nav-links'
    },
]

export const MenuItemsVisitor = [
    {
        title: 'Reseaux',
        url: '/reseaux',
        cName: 'nav-links'
    },
    {
        title: 'Boutique',
        url: '/boutique',
        cName: 'nav-links'
    },
    {
        title: "Se connecter",
        url: '/signin',
        cName: 'nav-links'
    },
]